# docs-as-code

##  PlantUML

```plantuml
@startuml component
actor client
node app
database db

db -> app
app -> client
@enduml
```

##  PlantUML

```plantuml
@startuml
!include test.puml
@enduml
```
